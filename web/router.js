import React, {Component} from 'react';
import { 
  HashRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import Home from './pages/home';
import Eslint from './pages/eslint';
import Vue from './pages/vue';

import './public/base.css';
import './public/prism.css';

export default class Routers extends Component {
  render(){
    return(
      <Router>
        <Switch>
          <Route exact path="/" exact component={Home} />
          <Route exact path="/eslint" exact component={Eslint} />
          <Route exact path="/vue" exact component={Vue} />
        </Switch>
      </Router>
    );
  }
}
