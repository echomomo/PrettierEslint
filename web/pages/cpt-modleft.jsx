import React, { Component } from 'react';

export default class HomeLeft extends Component {
  constructor(props) {
    super(props);
  }

  goto(name) {
    const oTmpWindow = window.open('_blank');
    if(name.toString().indexOf("vue/") != -1){
      oTmpWindow.location.href = this.props.sUrlBase + name.replace("vue/","") + ".html";
    }else{
      oTmpWindow.location.href = this.props.sUrlBase + name;
    }
  }

  render () {
    const {name, description, resion, valid} = this.props;
    let styName = "rule-name";
    if (valid.toString() === 'off'){
      styName += " rule-name-warn";
    }

    return (
      <div className="left-box">
        <p className={styName}><span onClick={this.goto.bind(this, name)} style={{"cursor": "pointer"}}>{name}</span></p>
        <p className="rule-rm">{description}</p>
        <p className="rule-resion">{resion}</p>
      </div>
    );
  }
}