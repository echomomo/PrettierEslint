import React, { Component } from 'react';
import ReactPrism from '@versant-digital/react-prismjs';

export default class HomeMid extends Component {
  constructor(props) {
    super(props);
  }

  render () {
    const {badExample} = this.props;

    return (
      <div className="mid-box mr-box-bg">
        <div className="pre-warn">
          <ReactPrism language="javascript">
        {badExample}
          </ReactPrism>
        </div>
      </div>
    );
  }
}