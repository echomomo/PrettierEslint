import React, { Component } from 'react';

export default class HomeLeft extends Component {
  constructor(props) {
    super(props);
  }

  // 父级事件回调
  hpHeaderHandle(oVal) {
    this.props.hpHeaderHandle(oVal);
  }

  btnChange (sVal){
    this.hpHeaderHandle({
      'type': 'filterLst',
      'val': sVal
    });
  }

  sltChange(e) {
    this.hpHeaderHandle({
      'type': 'toPage',
      'val': e.target.value
    });
  }

  render () {
    const {hpSltChange} = this.props;
    console.log("hpSltChange", this.props);

    const cmpRightBtn = hpSltChange !== '/' && (
      <React.Fragment>
        <div className="hd-btn hb-off" onClick={this.btnChange.bind(this, "off")}>显示禁用</div>
        <div className="hd-btn hb-on" onClick={this.btnChange.bind(this, "on")}>显示启用</div>
        <div className="hd-btn hb-all" onClick={this.btnChange.bind(this, "all")}>显示全部</div>
      </React.Fragment>
    );

    return (
      <React.Fragment>
        <header className="fx fx-justify-end">
          <select
            className="header-top-select"
            value={hpSltChange}
            onChange={this.sltChange.bind(this)}
          >
            <option value="/">Home</option>
            <option value="eslint">eslint</option>
            <option value="vue">vue</option>
          </select>
          
          { cmpRightBtn }
        </header>
      </React.Fragment>
    );
  }
}