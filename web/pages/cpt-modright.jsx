import React, { Component } from 'react';
import ReactPrism from '@versant-digital/react-prismjs';

export default class HomeRight extends Component {
  constructor(props) {
    super(props);
  }

  render () {
    const {goodExample} = this.props;

    return (
      <div className="right-box mr-box-bg">
        <div className="pre-good">
          <ReactPrism language="javascript">
        {goodExample}
          </ReactPrism>
        </div>
      </div>
    );
  }
}