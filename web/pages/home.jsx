import React, { Component, Fragment } from 'react';
import ReactPrism from '@versant-digital/react-prismjs';

// 头部
import HeaderTop from './cpt-header';


import '../public/home.css';
import inspections from '../public/webstorm-inspections.png';
import eslintOn from '../public/webstorm-eslint-on.png';
import inspectionsSucc from '../public/webstorm-inspections-succ.png';



class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      'sltChange': '/',

      'code1': `
    yarn add babel-eslint \\
        eslint \\
        eslint-config-prettier \\
        eslint-plugin-prettier-vue \\
        eslint-plugin-vue \\
        prettier \\
        vue-eslint-parser -D
    # 或
    cnpm install babel-eslint ... -D
      `,

      'code2':`
    "babel-eslint": "^10.1.0",
    "eslint": "^6.8.0",
    "eslint-config-prettier": "^6.10.1",
    "eslint-plugin-prettier-vue": "^2.0.2",
    "eslint-plugin-vue": "^6.2.2",
    "prettier": "^2.0.4",
    "vue-eslint-parser": "^7.0.0"
      `,

      'code3': `
    unzip ~/Downloads/eslintrc.js.zip
    mv -i ~/Downloads/dist/eslintrc-vue.js ~/{yourProject}/.eslintrc.js
      `,

      'code4': `
    "scripts": {
      "lint": "eslint --ext .js,.vue src",
      "lint-fix": "eslint --fix --ext .js,.vue src --cache"
    },
      `,

      'code5': `
    node_modules/*
      `,

      'code6': `
    {
      // 启用eslint
      "eslint.enable": true,
      // 禁用源校验
      "javascript.validate.enable": false,
      // 禁用vetur格式化功能及校验
      "vetur.format.enable": false,
      "vetur.validation.template": false,
      // 输出 ESLint 执行时的 log，ESLint 不生效的时候可以启用看看
      "eslint.trace.server": "messages",
      // 配置 ESLint 检查的文件类型，这个配置包括 .js, .jsx, .html, .vue
      "eslint.validate": [
      "javascript",
      "html",
      "vue"
      ],
      // 保存时自动格式化
      "editor.formatOnSave": true
    }
      `
    };
  }

  // header 交互数据
  hpHeaderHandle(oVal) {
    switch (oVal['type']) {
      case 'toPage':
        this.props.history.push({
          pathname: oVal['val']
        });
        break;
    }
  }


  render() {
    const { sltChange, code1, code2, code3, code4, code5, code6 } = this.state;

    return (
      <React.Fragment>

        <HeaderTop
          hpHeaderHandle={this.hpHeaderHandle.bind(this)}
          hpSltChange={sltChange}
        />
        <div className='home-warp'>

          <h1>Prettier && Eslint</h1>

          <h2 className='l1-title'>项目介绍</h2>

          <strong>问题：</strong>
          <p>想在团队中推行一定的代码规范，并给不符合规范的代码做检测和提示，多IDE工具能统一支持。</p>
          <br />

          <strong>方案：</strong>
          <p> 代码规范校验使用 ESLint，但是一开始 ESLint 只有检测告诉你哪里有问题，常常出现的情况就是一堆 warning，改起来很痛苦。后来 ESLint 提供了 `eslint --fix --ext .js,.vue src --cache` 的命令可以自动帮你修复一些不符合规范的代码。Prettier 是一个代码格式化工具，可以帮你把代码格式化成可读性更好的格式，最典型的就是一行代码过长的问题。 </p>
          <br />

          <strong>Eslint和Prettier区别：</strong>
          <p> 那 ESLint 和 Prettier 的区别是什么呢？eslint（包括其他一些 lint 工具）的主要功能包含代码格式的校验，代码质量的校验。而 Prettier 只是代码格式的校验（并格式化代码），不会对代码质量进行校验。代码格式问题通常指的是：单行代码长度、tab长度、空格、逗号表达式等问题。而代码质量问题指的是：未使用变量、三等号、全局变量声明等问题。 </p>
          <br />

          <strong>Eslint和Prettier配合使用：</strong>
          <p> 为什么要两者配合使用？因为，第一在 ESLint 推出 --fix 参数前，ESLint 并没有自动化格式代码的功能，要对一些格式问题做批量格式化只能用 Prettier 这样的工具。第二 ESLint 的规则并不能完全包含 Prettier 的规则，两者不是简单的谁替代谁的问题。但是在 ESLint 推出 --fix 命令行参数之后，如果你觉得 ESLint 提供的格式化代码够用了，也可以不使用 Prettier。 </p>
          <p> ESLint 和 Prettier 相互合作的时候有一些问题，对于他们交集的部分规则，ESLint 和 Prettier 格式化后的代码格式不一致。导致的问题是：当你用 Prettier 格式化代码后再用 ESLint 去检测，会出现一些因为格式化导致的 warning，鉴于以上问题，本项目采用方案是： </p>
          <p> 1. 引入 `Prettier` 作为 `Eslint` 插件先格式化一把，同时关闭所有提示问题。 </p>
          <p> 2. 运行 Prettier 之后，再使用 `eslint —fix` 格式化一把，这样把冲突的部分以 ESLint 的格式为标准覆盖掉，剩下的 warning 就都是代码质量问题了。 </p>
          <p> 注：引入 `vue.js` 风格指南中 [优先级 C：推荐](https://cn.vuejs.org/v2/style-guide/index.html#优先级-C：推荐) 标准。 </p>
          <br />

          <strong>优势：</strong>
          <p>1. 不接入 `webpack` 的 `eslint-loader` ，这样不会因为书写问题打断开发。 </p>
          <p>2. IDE工具支持提示校验，配合保存或IDE的 `eslint --fix` 直接快速辅助修复问题。 </p>
          <p>3. 可内部团队自定统一规则，生成配套规则和介绍页面。</p>
          <br />

          <h2 className='l1-title'>使用安装</h2>

          <strong>项目扩展包：</strong>
          <div className='hpre'>
            <ReactPrism language="markup">{ code1 }</ReactPrism>
          </div>

          <strong>版本参考：</strong>
          <div className='hpre'>
            <ReactPrism language="javascript">{ code2 }</ReactPrism>
          </div>

          <strong>下载：</strong>
          <p><a href='https://gitee.com/echomomo/PrettierEslint/blob/master/web/public/eslintrc.js.zip' target='_blank'> `eslintrc-vue.js` </a></p>
          <p>拷贝 `js` 文件到项目根目录，重命令为 `.eslintrc.js` 的隐藏文件。</p>
          <div className='hpre'><ReactPrism language="markup">{ code3 }</ReactPrism></div>


          <h2 className='l1-title'>package.json</h2>
          <p>配置项目 `检测脚本` 及 `修复脚本`。</p>
          <p>会对src项目下所有 `.js` `.vue` 的扩展类进行格式化及检查。</p>
          <div className='hpre'>
            <ReactPrism language="javascript">{ code4 }</ReactPrism>
          </div>

          <p>如果有第三个库类不需要处理可以根目录加入 `.eslintignore` 过行过滤</p>
          <div className='hpre'>
            <ReactPrism language="markup">{ code5 }</ReactPrism>
          </div>

          <h2 className='l1-title'>vscode</h2>
          <p>`.vscode/settings.json`  文件配置。</p>
          <div className='hpre'>
            <ReactPrism language="javascript">{ code6 }</ReactPrism>
          </div>

          <h2 className='l1-title'>webstorm</h2>

          <p>webstorm略复杂些，自带有一套eslint标准，会和本标准部分冲突，需要手动关闭。</p>

          <p>1. 禁用所有HTML 、JAVASCRIPT的校验规则（如果没有强迫症可以保留javascript的规则，会有更好的书写校验）。</p>
          <p><img src={inspections} className='web-img' /></p>

          <p>2. 单启用Eslint支持项目级的校验规则</p>
          <p><img src={eslintOn} className='web-img' /></p>

          <p>3. 完成结果</p>
          <p><img src={inspectionsSucc} className='web-img' /></p>

        </div>
      </React.Fragment>
    );
  }
}

export default Home;
