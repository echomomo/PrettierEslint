oEslintReco = { // eslint-recommended 开启的规则 50项
  'for-direction': 'on', // 禁止方向错误的for循环
  'getter-return': 'on', // getter必须有返回值，并且禁止返回空
  'no-async-promise-executor': 'on', // 禁止将async函数做为new Promise的回调函数
  'no-compare-neg-zero': 'on', // 禁止与负零进行比较
  'no-cond-assign': 'on', // 禁止在测试表达式中使用赋值语句，除非这个赋值语句被括号包起来了
  'no-constant-condition': 'on', // 禁止将常量作为分支条件判断中的测试表达式，但允许作为循环条件判断中的测试表达式
  'no-dupe-else-if': 'on', // 禁止if else的条件判断中出现重复的条件
  'no-dupe-keys': 'on', // 禁止在对象字面量中出现重复的键名
  'no-duplicate-case': 'on', // 禁止在switch语句中出现重复测试表达式的case
  'no-empty': 'on', // 禁止出现空代码块，允许catch为空代码块
  'no-empty-character-class': 'on', // 禁止在正则表达式中使用空的字符集[]
  'no-ex-assign': 'on', // 禁止将catch的第一个参数error重新赋值
  'no-extra-boolean-cast': 'on', // 禁止不必要的布尔类型转换
  'no-extra-semi': 'on', // 则禁用不必要的分号
  'no-func-assign': 'on', // 禁止将一个函数声明重新赋值
  'no-import-assign': 'on', // 禁止对导入的模块进行赋值
  'no-inner-declarations': 'on', // 禁止在if代码块内出现函数声明
  'no-invalid-regexp': 'on', // 禁止在RegExp构造函数中出现非法的正则表达式
  'no-irregular-whitespace': 'on', // 禁止使用特殊空白符（比如全角空格），除非是出现在字符串、正则表达式或模版字符串中
  'no-misleading-character-class': 'on', // 禁止正则表达式中使用肉眼无法区分的特殊字符
  'no-obj-calls': 'on', // 禁止将Math,JSON或Reflect直接作为函数调用
  'no-regex-spaces': 'on', // 禁止在正则表达式中出现连续的空格
  'no-setter-return': 'on', // 禁止 setter 有返回值
  'no-sparse-arrays': 'on', // 禁止在数组中出现连续的逗号
  'no-unexpected-multiline': 'on', // 该规则禁止使用令人困惑的多行表达式
  'no-unreachable': 'on', // 禁止在 return, throw, break 或 continue 之后还有代码
  'no-unsafe-finally': 'on', // 禁止在 finally 中出现 return, throw, break 或 continue
  'no-unsafe-negation': 'on', // 禁止在 in 或 instanceof 操作符的左侧变量前使用感叹号
  'use-isnan': 'on', // 必须使用 isNaN(foo) 而不是 foo === NaN
  'valid-typeof': 'on', // typeof表达式比较的对象必须是'undefined'，'object'，'boolean'，'number'，'string'，'function'，'symbol'，或'bigint'
  'no-case-declarations': 'on', // switch的case内有变量定义的时候，必须使用大括号将case内变成一个代码块 !!!!
  'no-empty-pattern': 'on', // 禁止解构赋值中出现空{}或[]
  'no-fallthrough': 'on', // switch的case内必须有break,return或throw，空的case除外
  'no-global-assign': 'on', // 禁止对全局变量赋值
  'no-redeclare': 'on', // 禁止重复定义变量
  'no-self-assign': 'on', // 禁止将自己赋值给自己
  'no-useless-catch': 'on', // 禁止在 catch 中仅仅只是把错误 throw 出去
  'no-with': 'on', // 禁止使用 with
  'no-delete-var': 'on', // delete 的目的是删除对象的属性。使用 delete 操作删除一个变量可能会导致意外情况发生。
  'no-shadow-restricted-names': 'on', // 禁止使用保留字作为变量名
  'no-undef': 'on', // 禁止使用未定义的变量
  'no-unused-vars': 'on', // 已定义的变量必须使用
  'no-mixed-spaces-and-tabs': 'on', // 禁止使用 空格 和 tab 混合缩进
  'constructor-super': 'on', // constructor中必须有super
  'no-class-assign': 'on', // 禁止对已定义的class重新赋值
  'no-const-assign': 'on', // 禁止对使用const定义的常量重新赋值
  'no-dupe-class-members': 'on', // 禁止重复定义类的成员
  'no-new-symbol': 'on', // 禁止使用new来生成Symbol
  'no-this-before-super': 'on', // 禁止在 super 被调用之前使用 this 或 super
  'require-yield': 'on', // generator 函数内必须有 yield
  // eslint-recommended 禁用掉的规则 8项
  'no-control-regex': 'off', // 禁止在正则表达式中出现Ctrl键的ASCII表示，即禁止使用/\x1f/
  'no-debugger': 'off', // 禁止使用debugger
  'no-dupe-args': 'off', // 禁止在函数参数中出现重复名称的参数编译阶段就会报错了
  'no-prototype-builtins': 'off', // 禁止使用 hasOwnProperty, isPrototypeOf 或 propertyIsEnumerable
  'require-atomic-updates': 'off', // 禁止将await或yield的结果做为运算符的后面项
  'no-octal': 'off', // 禁止使用0开头的数字表示八进制数
  'no-unused-labels': 'off', // 禁止出现没用到的label
  'no-useless-escape': 'off', // 禁止出现没必要的转义
};
oEslintAdd = { // 补充 recommended 的规则 58项
  'accessor-pairs': 'on', // setter必须有对应的getter，getter可以没有对应的setter
  'complexity': 'on', // 禁止函数的循环复杂度超过20
  'curly': 'on', // 不允许省略花括号
  'func-name-matching': 'on', // 函数赋值给变量的时候，函数名必须与变量名一致
  'grouped-accessor-pairs': 'on', // setter和getter必须写在一起
  'indent': 'on', // 缩进2格
  'max-depth': 'on', // 代码块嵌套的深度禁止超过5层
  'max-nested-callbacks': 'on', // 回调函数嵌套禁止超过5层，多了请用async await替代
  'max-params': 'on', // 函数的参数禁止超过5个
  'new-cap': 'on', // new后面的类名必须首字母大写
  'no-array-constructor': 'on', // 禁止使用Array构造函数时传入的参数超过一个
  'no-caller': 'on', // 禁止使用caller或callee
  'no-constructor-return': 'on', // 禁止在构造函数中返回值
  'no-dupe-else-if': 'on', // 禁止if else的条件判断中出现重复的条件
  'no-duplicate-imports': 'on', // 禁止重复导入模块
  'no-eq-null': 'on', // 禁止使用foo==null，必须使用foo===null
  'no-eval': 'on', // 禁止使用eval
  'no-extra-bind': 'on', // 禁止出现没必要的bind
  'no-implicit-coercion': 'on', // 禁止使用~+等难以理解的类型转换，仅允许使用!! +
  'no-implied-eval': 'on', // 禁止在setTimeout或setInterval中传入字符串
  'no-import-assign': 'on', // 禁止对导入的模块进行赋值
  'no-iterator': 'on', // 禁止使用__iterator__
  'no-label-var': 'on', // 禁止label名称与已定义的变量重复
  'no-labels': 'on', // 禁止使用label
  'no-multi-str': 'on', // 禁止使用 \ 来换行字符串
  'no-new-func': 'on', // 禁止使用new Function
  'no-new-object': 'on', // 禁止直接new Object
  'no-new-wrappers': 'on', // 禁止使用new来生成String,Number或Boolean
  'no-new': 'on', // 禁止直接new一个类而不赋值
  'no-param-reassign': 'on', // *** 禁止对函数的参数重新赋值
  'no-proto': 'on', // 禁止使用 __proto__
  'no-return-assign': 'on', // 禁止在return语句里赋值
  'no-return-await': 'on', // 禁止在return语句里使用await
  'no-self-compare': 'on', // 禁止将自己与自己比较
  'no-sequences': 'on', // 禁止使用逗号操作符
  'no-template-curly-in-string': 'on', // 禁止在普通字符串中出现模版字符串里的变量形式
  'no-throw-literal': 'on', // 禁止 throw 字面量，必须 throw 一个 Error 对象
  'no-undef-init': 'on', // 禁止将 undefined 赋值给变量
  'no-unmodified-loop-condition': 'on', // 循环内必须对循环条件中的变量有修改
  'no-unused-expressions': 'on', // 禁止无用的表达式
  'no-use-before-define': 'on', // 变量必须先定义后使用
  'no-useless-call': 'on', // 禁止出现没必要的call或apply
  'no-useless-computed-key': 'on', // 禁止出现没必要的计算键名
  'no-useless-concat': 'on', // 禁止出现没必要的字符串连接
  'no-useless-rename': 'on', // 禁止解构赋值时出现同样名字的的重命名，比如let{foo:foo}=bar;
  'no-var': 'on', // 禁止使用 var
  'no-void': 'on', // 禁止使用void
  'one-var': 'on', // 禁止变量申明时用逗号一次申明多个
  'prefer-regex-literals': 'on', // 优先使用正则表达式字面量，而不是 RegExp 构造函数
  'radix': 'on', // parseInt 必须传入第二个参数
  'semi': 'on', // 要求或禁止使用分号代替 ASI
  'spaced-comment': 'on', // 注释的斜线或后必须有空格
  'strict': 'on', // 禁止使用 'strict';
  'symbol-description': 'on', // 创建 Symbol 时必须传入参数
  'yoda': 'on', // 必须使用 if (foo === 5) 而不是 if (5 === foo)，不强制用全等。

  'camelcase': 'off', // 变量名必须是camelcase风格的
  'eqeqeq': 'off', // 必须使用===或!==，禁止使用==或!=
  'no-restricted-syntax': 'off', // 禁止使用指定的语法
  'sort-keys': 'off', // 对象字面量的键名必须排好序
};

oPluginVue = { // vue 74 项规则
  'vue/attribute-hyphenation': 'off', // 限制自定义组件的属性风格
  'vue/attributes-order': 'on', // 标签属性必须按规则排序
  'vue/camelcase': 'off', // 变量名必须是 camelcase 风格的
  'vue/comment-directive': 'on', // 支持在模版中使用 eslint-disable-next-line 等注释
  'vue/component-definition-name-casing': 'on', // 组件的 name 属性必须符合 PascalCase
  'vue/component-name-in-template-casing': 'off', // 限制组件名的风格
  'vue/component-tags-order': 'on', // 组件中必须按照 <template>, <script>, <style> 排序，标签的顺序保持一致
  'vue/eqeqeq': 'off', // 必须使用 === 或 !==，禁止使用 == 或 !=
  'vue/html-self-closing': 'on', // 执行自动关闭样式
  'vue/jsx-uses-vars': 'on', // 修复 no-unused-vars 不检查 jsx 的问题
  'vue/match-component-file-name': 'off', // 组件名称必须和文件名一致
  'vue/max-attributes-per-line': 'on', // 强制每行属性的最大数量，最多不超4个。
  'vue/name-property-casing': 'off', // 限制组件的 name 属性的值的风格
  'vue/no-async-in-computed-properties': 'on', // 计算属性禁止包含异步方法
  'vue/no-boolean-default': 'off', // 禁止给布尔值 props 添加默认值
  'vue/no-deprecated-scope-attribute': 'on', // 禁用已废弃的 scope 属性
  'vue/no-deprecated-slot-attribute': 'off', // 使用 v-slot 替代已废弃的 slot
  'vue/no-deprecated-slot-scope-attribute': 'off', // 禁用已废弃的 slot-scope
  'vue/no-dupe-keys': 'on', // 禁止重复的键名
  'vue/no-duplicate-attributes': 'on', // 禁止出现重复的属性
  'vue/no-empty-pattern': 'on', // 禁止解构赋值中出现空 {} 或 []
  'vue/no-irregular-whitespace': 'on', // 禁止使用特殊空白符（比如全角空格），除非是出现在字符串、正则表达式、模版字符串中或 HTML 内容中
  'vue/no-parsing-error': 'on', // *** 禁止出现语法错误
  'vue/no-reserved-component-names': 'on', // 组件的 name 属性静止使用保留字
  'vue/no-reserved-keys': 'on', // 禁止覆盖保留字
  'vue/no-restricted-syntax': 'off', // 禁止使用指定的语法
  'vue/no-shared-component-data': 'off', // 组件的 data 属性的值必须是一个函数
  'vue/no-side-effects-in-computed-properties': 'off', // 禁止在计算属性中对属性修改
  'vue/no-static-inline-styles': 'off', // 禁止使用 style 属性
  'vue/no-template-key': 'off', // 禁止 <template> 使用 key 属性
  'vue/no-template-shadow': 'off', // 模版中的变量名禁止与前一个作用域重名
  'vue/no-textarea-mustache': 'on', // 禁止在 <textarea> 中出现模版语法 {{message}}
  'vue/no-unsupported-features': 'off', // 当你的 vue 版本较老时，禁用还未支持的语法
  'vue/no-unused-components': 'on', // 禁止定义在 components 中的组件未使用
  'vue/no-unused-vars': 'on', // 模版中已定义的变量必须使用
  'vue/no-use-v-if-with-v-for': 'on', // *** 禁止在同一个元素上使用 v-if 和 v-for 指令
  'vue/no-v-html': 'off', // 禁止使用 v-html
  'vue/order-in-components': 'on', // 组件的属性必须为一定的顺序
  'vue/padding-line-between-blocks': 'off', // <template> <script> <style> 之间必须由空行 
  'vue/prop-name-casing': 'off', // props 必须用驼峰式
  'vue/require-component-is': 'on', // <component> 必须有绑定的组件
  'vue/require-default-prop': 'off', // props 如果不是 required 的字段，必须有默认值
  'vue/require-direct-export': 'off', // 必须直接使用 export default 导出组件
  'vue/require-name-property': 'off', // 组件必须包含 name 属性
  'vue/require-prop-type-constructor': 'off', // props 的取值必须是基本类型的构造函数，而不是字符串
  'vue/require-prop-types': 'off', // prop 必须有类型限制
  'vue/require-render-return': 'on', // render 函数必须有返回值
  'vue/require-v-for-key': 'on', // v-for 指令的元素必须有 v-bind:key
  'vue/require-valid-default-prop': 'off', // prop 的默认值必须匹配它的类型
  'vue/return-in-computed-property': 'on', // 计算属性必须有返回值
  'vue/sort-keys': 'off', // props 的键名必须排好序
  'vue/static-class-names-order': 'off', // class 的值必须按字母排序
  'vue/this-in-template': 'on', // 禁止在模版中用 this
  'vue/use-v-on-exact': 'on', // 当一个节点上出现两个 v-on:click 时，其中一个必须为 exact
  'vue/v-bind-style': 'off', // 使用缩写的 : 而不是 v-bind:
  'vue/v-on-function-call': 'on', // 禁止在 v-on 的值中调用函数
  'vue/v-on-style': 'off', // 使用缩写的 @click 而不是 v-on:click
  'vue/v-slot-style': 'off', // 使用缩写的 #one 而不是 v-slot:one
  'vue/valid-template-root': 'on', // template 的根节点必须合法
  'vue/valid-v-bind-sync': 'on', // v-bind:foo.sync 指令必须合法
  'vue/valid-v-bind': 'on', // v-bind 指令必须合法
  'vue/valid-v-cloak': 'on', // v-cloak 指令必须合法
  'vue/valid-v-else-if': 'on', // v-else-if 指令必须合法
  'vue/valid-v-else': 'on', // v-else 指令必须合法
  'vue/valid-v-for': 'on', // v-for 指令必须合法
  'vue/valid-v-html': 'on', // v-html 指令必须合法
  'vue/valid-v-if': 'on', // v-if 指令必须合法
  'vue/valid-v-model': 'on', // v-model 指令必须合法
  'vue/valid-v-on': 'on', // v-on 指令必须合法
  'vue/valid-v-once': 'on', // v-once 指令必须合法
  'vue/valid-v-pre': 'on', // v-pre 指令必须合法
  'vue/valid-v-show': 'on', // v-show 指令必须合法
  'vue/valid-v-slot': 'on', // v-slot 指令必须合法
  'vue/valid-v-text': 'on', // v-text 指令必须合法
}


module.exports = {
  oEslintReco,
  oEslintAdd,
  oPluginVue,
}