module.exports = {
  name: "symbol-description",
  value: "error",
  goodExample: `
    const foo = Symbol('foo');
  `,
  badExample: `
    const foo = Symbol();
  `,
  resion: "",
  description: `创建 Symbol 时必须传入参数`,
  valid: "on",
};
