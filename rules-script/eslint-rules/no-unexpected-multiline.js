module.exports = {
  name: 'no-unexpected-multiline',
  value: "error",
  goodExample: `
    var foo = bar;
    (1 || 2).baz();
    
    var foo = bar
    ;(1 || 2).baz()
    
    var hello = 'world';
    [1, 2, 3].forEach(addNumber);
    
    var hello = 'world'
    void [1, 2, 3].forEach(addNumber);
    
    let x = function() {};
    \`hello\`
    
    let tag = function() {}
    tag \`hello\`
  `,
  badExample: `
    var foo = bar
    (1 || 2).baz();
    
    var hello = 'world'
    [1, 2, 3].forEach(addNumber);
    
    let x = function() {}
    \`hello\`
    
    let x = function() {}
    x
    \`hello\`
    
    let x = foo
    /regex/g.test(bar)
  `,
  resion: '',
  description: `该规则禁止使用令人困惑的多行表达式`,
  valid: 'on'
};
