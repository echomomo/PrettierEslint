module.exports = {
  name: "no-empty",
  value: ['error', {allowEmptyCatch: true}],
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止出现空代码块，允许catch为空代码块`,
  valid: "on",
};
