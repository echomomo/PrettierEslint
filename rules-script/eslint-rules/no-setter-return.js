module.exports = {
  name: "no-setter-return",
  value: "error",
  goodExample: `
    const foo = {
      set bar(value) {
        this.barValue = 'bar ' + value;
      }
    };
  `,
  badExample: `
    const foo = {
      set bar(value) {
        this.barValue = 'bar ' + value;
        return this.barValue;
      }
    };
  `,
  resion: "",
  description: `禁止 setter 有返回值`,
  valid: "on",
};
