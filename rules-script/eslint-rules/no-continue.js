module.exports = {
  name: "no-continue",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用continue`,
  valid: "off",
};
