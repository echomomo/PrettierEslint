module.exports = {
  name: "no-unneeded-ternary",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "后者表达的更清晰",
  description: `必须使用 !a 替代 a ? false : true`,
  valid: "off",
};
