module.exports = {
  name: "valid-typeof",
  value: "error",
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `typeof表达式比较的对象必须是'undefined'，'object'，'boolean'，'number'，'string'，'function'，'symbol'，或'bigint'`,
  valid: "on",
};
