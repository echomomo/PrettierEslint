module.exports = {
  name: "no-sequences",
  value: "error",
  goodExample: `
    doSomething();
    foo = 1;
  `,
  badExample: `
    foo = doSomething(), 1;
  `,
  resion: "",
  description: `禁止使用逗号操作符`,
  valid: "on",
};
