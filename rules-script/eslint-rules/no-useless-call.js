module.exports = {
  name: "no-useless-call",
  value: "error",
  goodExample: `
    foo.call(bar, 1, 2, 3);
    foo.apply(bar, [1, 2, 3]);

    foo.bar.call(baz, 1, 2, 3);
    foo.bar.apply(baz, [1, 2, 3]);
  `,
  badExample: `
    foo.call(null, 1, 2, 3); // foo(1, 2, 3)
    foo.apply(null, [1, 2, 3]); // foo(1, 2, 3)

    foo.bar.call(foo, 1, 2, 3); // foo.bar(1, 2, 3);
    foo.bar.apply(foo, [1, 2, 3]); // foo.bar(1, 2, 3);
  `,
  resion: "",
  description: `禁止出现没必要的call或apply`,
  valid: "on",
};
