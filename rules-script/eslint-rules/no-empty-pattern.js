module.exports = {
  name: "no-empty-pattern",
  value: "error",
  goodExample: `
    const { bar } = foo;
  `,
  badExample: `
    const {} = foo;
  `,
  resion: "",
  description: `禁止解构赋值中出现空{}或[]`,
  valid: "on",
};
