module.exports = {
  name: "no-labels",
  value: "error",
  goodExample: `
  for (let i = 0; i < 5; i++) {
    if (i === 1) {
        continue;
    }
    console.log(i);
  }
  // 0 2 3 4
  `,
  badExample: `
    loop:
    for (let i = 0; i < 5; i++) {
      if (i === 1) {
        continue loop;
      }
      console.log(i);
    }
    // 0 2 3 4
  `,
  resion: "",
  description: `禁止使用label`,
  valid: "on",
};
