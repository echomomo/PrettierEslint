module.exports = {
  name: "no-new",
  value: "error",
  goodExample: ``,
  badExample: ``,
  resion: "new应该作为创建一个类的实例的方法，所以不能不赋值",
  description: `禁止直接new一个类而不赋值`,
  valid: "on",
};
