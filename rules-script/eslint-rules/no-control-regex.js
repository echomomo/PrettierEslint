module.exports = {
  name: "no-control-regex",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "几乎不会遇到这种场景",
  description: `禁止在正则表达式中出现Ctrl键的ASCII表示，即禁止使用/\\x1f/`,
  valid: "off",
};
