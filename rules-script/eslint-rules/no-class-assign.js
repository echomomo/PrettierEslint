module.exports = {
  name: "no-class-assign",
  value: "error",
  goodExample: `
    class Foo {}
  `,
  badExample: `
    class Foo {}
    Foo = {}
  `,
  resion: "",
  description: `禁止对已定义的class重新赋值`,
  valid: "on",
};
