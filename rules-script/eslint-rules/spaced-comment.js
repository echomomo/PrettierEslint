module.exports = {
  name: "spaced-comment",
  value: ['warn', 'always', {
    block: {
      exceptions: [''],
      balanced: true
    }
  }],
  goodExample: `
    // foo
    /* bar */
    /** baz */
  `,
  badExample: `
    //foo
    /*bar */
    /**baz */
  `,
  resion: "",
  description: `注释的斜线或后必须有空格`,
  valid: "on",
};
