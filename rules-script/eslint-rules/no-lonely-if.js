module.exports = {
  name: "no-lonely-if",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "可以把逻辑表达的更清楚",
  description: `禁止else中只有一个单独的if`,
  valid: "off",
};
