module.exports = {
  name: "constructor-super",
  value: "error",
  goodExample: `
    class Foo extends Bar {
      constructor() {
        super();
      }
    }
  `,
  badExample: `
    class Foo extends Bar {
      constructor() {}
    }
  `,
  resion: "",
  description: `constructor中必须有super`,
  valid: "on",
};
