module.exports = {
  name: "use-isnan",
  value: "error",
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `必须使用 isNaN(foo) 而不是 foo === NaN`,
  valid: "on",
};
