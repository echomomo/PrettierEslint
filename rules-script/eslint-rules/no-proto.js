module.exports = {
  name: "no-proto",
  value: "error",
  goodExample: `
    const foo = Object.getPrototypeOf(bar);
    Object.setPrototypeOf(bar, baz);
  `,
  badExample: `
    const foo = bar.__proto__;
    bar.__proto__ = baz;
  `,
  resion: "__proto__ 是已废弃的语法, getPrototypeOf, setPrototypeOf ES5已经支持",
  description: `禁止使用 __proto__`,
  valid: "on",
};
