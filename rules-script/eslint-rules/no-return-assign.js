module.exports = {
  name: 'no-return-assign',
  value: ['error', 'always'],
  goodExample: `
    function foo() {
      bar = 1;
      return bar;
    }
  `,
  badExample: `
    function foo() {
      return (bar = 1);
    }
  `,
  resion: '配置：["error", "always"]',
  description: `禁止在return语句里赋值`,
  valid: 'on',
};
