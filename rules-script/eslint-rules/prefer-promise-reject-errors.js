module.exports = {
  name: 'prefer-promise-reject-errors',
  value: 'off',
  goodExample: `
    Promise.reject(new Error('foo'));

    new Promise((resolve, reject) => {
      reject(new Error('foo'));
    });
  `,
  badExample: `
    Promise.reject('foo');

    new Promise((resolve, reject) => {
      reject();
    });

    new Promise((resolve, reject) => {
      reject('foo');
    });
  `,
  resion: '有时只是简单流转，不做强制。',
  description: `Promise 的 reject 中必须传入 Error 对象，而不是字面量`,
  valid: 'off',
};
