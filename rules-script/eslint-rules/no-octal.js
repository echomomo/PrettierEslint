module.exports = {
  name: "no-octal",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "编译阶段就会报错了",
  description: `禁止使用0开头的数字表示八进制数`,
  valid: "off",
};
