module.exports = {
  name: "yoda",
  value: ['error','never', {
    onlyEquality: true
  }],
  goodExample: `
    if (5 & value) {
      // ...
    }
    if (value === "red") {
      // ...
    }
  `,
  badExample: `
    if ("red" === color) {
      // ...
    }
    if (true == flag) {
      // ...
    }
    if (5 > count) {
      // ...
    }
    if (-1 < str.indexOf(substr)) {
      // ...
    }
    if (0 <= x && x < 1) {
      // ...
    }
  `,
  resion: "兼容返回数据类型被改变，导致线上报错。",
  description: `必须使用 if (foo === 5) 而不是 if (5 === foo)，不强制用全等。`,
  valid: "on",
};
