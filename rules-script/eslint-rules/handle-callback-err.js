module.exports = {
  name: "handle-callback-err",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "它是通过字符串匹配来判断函数参数err的，不准确",
  description: `callback中的err必须被处理`,
  valid: "off",
};
