module.exports = {
  name: "no-implicit-coercion",
  value: ['error', {
    allow: ['!!', '+']
  }],
  goodExample: `
    const b = foo.indexOf('.') !== -1;
    const n = Number(foo);
    const m = Number(foo);
    const s = String(foo);
    foo = String(foo);

    const c = !!foo;
    const n = +foo;
  `,
  badExample: `
    const b = ~foo.indexOf('.');
    const m = 1 * foo;
    const s = '' + foo;
    foo += '';
  `,
  resion: "",
  description: `禁止使用~+等难以理解的类型转换，仅允许使用!! +`,
  valid: "on",
};
