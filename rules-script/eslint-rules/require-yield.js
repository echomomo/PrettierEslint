module.exports = {
  name: "require-yield",
  value: "error",
  goodExample: `
    function* foo() {
      yield 1;
      return 2;
    }
  `,
  badExample: `
    function* foo() {
      return 1;
    }
  `,
  resion: "",
  description: `generator 函数内必须有 yield`,
  valid: "on",
};
