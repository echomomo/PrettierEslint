module.exports = {
  name: "no-obj-calls",
  value: "error",
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止将Math,JSON或Reflect直接作为函数调用`,
  valid: "on",
};
