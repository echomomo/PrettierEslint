module.exports = {
  name: "one-var",
  value: ['error', 'never'],
  goodExample: `
    let foo;
    let bar;
    const baz = 1;
    const qux = 2;
  `,
  badExample: `
    let foo, bar;
    const baz = 1, qux = 2;
  `,
  resion: "",
  description: `禁止变量申明时用逗号一次申明多个`,
  valid: "on",
};
