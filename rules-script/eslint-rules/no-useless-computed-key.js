module.exports = {
  name: "no-useless-computed-key",
  value: "error",
  goodExample: `
    const foo = {
      1: 1,
      bar: 'bar'
    };
  `,
  badExample: `
    const foo = {
      ['1']: 1,
      ['bar']: 'bar'
    };
  `,
  resion: "",
  description: `禁止出现没必要的计算键名`,
  valid: "on",
};
