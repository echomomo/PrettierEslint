module.exports = {
  name: 'no-regex-spaces',
  value: 'error',
  goodExample: `
    const reg1 = /foo {3}bar/;
    const reg2 = new RegExp('foo {3}bar');
  `,
  badExample: `
    const reg1 = /foo   bar/;
    const reg2 = new RegExp('foo   bar');
  `,
  resion: '',
  description: `禁止在正则表达式中出现连续的空格`,
  valid: 'on',
};
