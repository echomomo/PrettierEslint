module.exports = {
  name: "no-nested-ternary",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用嵌套的三元表达式，比如a?b:c?d:e`,
  valid: "off",
};
