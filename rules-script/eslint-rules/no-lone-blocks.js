module.exports = {
  name: "no-lone-blocks",
  value: "off",
  goodExample: ``,
  badExample: ``,
  resion: "可能会有特殊场景需要，暂不强制处理。",
  description: `禁止使用没必要的{}作为代码块`,
  valid: "off",
};
