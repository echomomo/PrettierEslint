module.exports = {
  name: "no-multi-assign",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止连续赋值，比如: foo = bar = 1`,
  valid: "off",
};
