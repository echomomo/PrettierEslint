module.exports = {
  name: "no-unsafe-finally",
  value: "error",
  goodExample: `
    function foo() {
      try {
        return 1;
      } finally {
        console.log(2);
      }
    }
  `,
  badExample: `
    function foo() {
      try {
        return 1;
      } finally {
        // finally 会在 try 之前执行，故会 return 2
        return 2;
      }
    }
  `,
  resion: "finally 中的语句会在 try 之前执行",
  description: `禁止在 finally 中出现 return, throw, break 或 continue`,
  valid: "on",
};
