module.exports = {
  name: "no-mixed-requires",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `相同类型的require必须放在一起`,
  valid: "off",
};
