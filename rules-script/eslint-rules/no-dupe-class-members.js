module.exports = {
  name: "no-dupe-class-members",
  value: "error",
  goodExample: `
    class Foo {
      bar() {}
      baz() {}
    }
  `,
  badExample: `
    class Foo {
      bar() {}
      bar() {}
    }
  `,
  resion: "",
  description: `禁止重复定义类的成员`,
  valid: "on",
};
