module.exports = {
  name: "lines-between-class-members",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "有时为了紧凑需要挨在一起，有时为了可读性需要空一行",
  description: `类的成员之间是否需要空行`,
  valid: "off",
};
