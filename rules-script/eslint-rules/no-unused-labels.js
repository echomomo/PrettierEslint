module.exports = {
  name: "no-unused-labels",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "已经禁止使用label了",
  description: `禁止出现没用到的label`,
  valid: "off",
};
