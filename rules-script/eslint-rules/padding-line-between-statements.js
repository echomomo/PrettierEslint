module.exports = {
  name: "padding-line-between-statements",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `限制语句之间的空行规则，比如变量定义完之后必须要空行`,
  valid: "off",
};
