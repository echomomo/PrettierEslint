module.exports = {
  name: "default-param-last",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `有默认值的参数必须放在函数参数的末尾`,
  valid: "off",
};
