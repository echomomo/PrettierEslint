module.exports = {
  name: "no-ternary",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用三元表达式`,
  valid: "off",
};
