module.exports = {
  name: "no-process-exit",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用process.exit(0)`,
  valid: "off",
};
