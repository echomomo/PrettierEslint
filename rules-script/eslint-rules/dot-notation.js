module.exports = {
  name: "dot-notation",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "当需要写一系列属性的时候，可以更统一",
  description: `禁止使用foo['bar']，必须写成foo.bar`,
  valid: "off",
};
