module.exports = {
  name: "prefer-regex-literals",
  value: "warn",
  goodExample: `
    /abc/;
    /\./g;
    new RegExp(prefix + 'abc');
  `,
  badExample: `
    new RegExp('abc');
    new RegExp('\\.', 'g');
  `,
  resion: "",
  description: `优先使用正则表达式字面量，而不是 RegExp 构造函数`,
  valid: "on",
};
