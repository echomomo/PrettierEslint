module.exports = {
  name: "no-process-env",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "对编译阶段引入处理有用处",
  description: `禁止使用 process.env.NODE_ENV`,
  valid: "off",
};
