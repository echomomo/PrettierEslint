module.exports = {
  name: "strict",
  value: ['error', 'never'],
  goodExample: `
    function foo() {}
  `,
  badExample: `
    'use strict';
    function foo() {
      'use strict';
    }
  `,
  resion: "",
  description: `禁止使用 'strict';`,
  valid: "on",
};
