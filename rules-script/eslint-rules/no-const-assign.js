module.exports = {
  name: "no-const-assign",
  value: "error",
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止对使用const定义的常量重新赋值`,
  valid: "on",
};
