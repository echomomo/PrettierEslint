module.exports = {
  name: "no-fallthrough",
  value: "error",
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `switch的case内必须有break,return或throw，空的case除外`,
  valid: "on",
};
