module.exports = {
  name: "no-empty-character-class",
  value: "error",
  goodExample: `
    const reg = /abc[a-z]/;
  `,
  badExample: `
    const reg = /abc[]/;
  `,
  resion: "",
  description: `禁止在正则表达式中使用空的字符集[]`,
  valid: "on",
};
