module.exports = {
  name: "no-implied-eval",
  value: "error",
  goodExample: `
    setTimeout(() => {
      console.log('Hello World');
    }, 1000);
  `,
  badExample: `
    setTimeout('alert("Hello World");', 1000);
  `,
  resion: "",
  description: `禁止在setTimeout或setInterval中传入字符串`,
  valid: "on",
};
