module.exports = {
  name: "no-use-before-define",
  value: ['error', {
    variables: false,
    functions: false,
    classes: false}
  ],
  goodExample: `
    (() => {
      console.log(foo);
    })();
    const foo = 1;
    console.log(foo);

    bar();
    function bar() {}

    (() => {
      new Baz();
    })();
    class Baz {}
    new Baz();
  `,
  badExample: `
    console.log(foo);
    const foo = 1;

    new Baz();
    class Baz {}
  `,
  resion: "",
  description: `变量必须先定义后使用`,
  valid: "on",
};
