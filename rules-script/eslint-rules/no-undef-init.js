module.exports = {
  name: "no-undef-init",
  value: "error",
  goodExample: `
    let foo;
  `,
  badExample: `
    let foo = undefined;
  `,
  resion: "",
  description: `禁止将 undefined 赋值给变量`,
  valid: "on",
};
