module.exports = {
  name: "no-multi-str",
  value: "error",
  goodExample: `
    const foo = \`Line 1
    Line 2\`;
  `,
  badExample: `
    const foo = 'Line 1\
    Line 2';
  `,
  resion: "",
  description: `禁止使用 \\ 来换行字符串`,
  valid: "on",
};
