module.exports = {
  name: "no-dupe-args",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止在函数参数中出现重复名称的参数编译阶段就会报错了`,
  valid: "off",
};
