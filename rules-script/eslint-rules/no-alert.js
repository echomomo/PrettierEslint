module.exports = {
  name: "no-alert",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用alert`,
  valid: "off",
};
