module.exports = {
  name: "no-extend-native",
  value: "error",
  goodExample: `
    function flat(arr) {
      // do something
    }

    flat([1, [2, 3]]);
  `,
  badExample: `
    Array.prototype.flat = function () {
      // do something
    };

    [1, [2, 3]].flat();
  `,
  resion: "修改原生对象可能会与将来版本的js冲突",
  description: `禁止修改原生对象`,
  valid: "on",
};
