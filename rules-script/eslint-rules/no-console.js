module.exports = {
  name: "no-console",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用console`,
  valid: "off",
};
