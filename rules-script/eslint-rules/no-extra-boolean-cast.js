module.exports = {
  name: "no-extra-boolean-cast",
  value: "error",
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止不必要的布尔类型转换`,
  valid: "on",
};
