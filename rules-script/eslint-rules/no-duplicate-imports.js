module.exports = {
  name: "no-duplicate-imports",
  value: "error",
  goodExample: `
    import { readFile, writeFile } from 'fs';
  `,
  badExample: `
    import { readFile } from 'fs';
    import { writeFile } from 'fs';
  `,
  resion: "",
  description: `禁止重复导入模块`,
  valid: "on",
};
