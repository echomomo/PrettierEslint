module.exports = {
  name: "consistent-return",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "缺少 TypeScript 的支持，类型判断是不准确的",
  description: `禁止函数在不同分支返回不同类型的值`,
  valid: "off",
};
