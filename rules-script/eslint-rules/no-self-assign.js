module.exports = {
  name: "no-self-assign",
  value: "error",
  goodExample: `
    foo = bar;
  `,
  badExample: `
    foo = foo;
  `,
  resion: "",
  description: `禁止将自己赋值给自己`,
  valid: "on",
};
