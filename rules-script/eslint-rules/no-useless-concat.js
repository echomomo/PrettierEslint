module.exports = {
  name: "no-useless-concat",
  value: "error",
  goodExample: `
    const foo = 'fo';
    const bar = 1 + \`ar\`;
  `,
  badExample: `
    const foo = 'f' + 'oo';
    const bar = \`b\` + \`ar\`;
  `,
  resion: "",
  description: `禁止出现没必要的字符串连接`,
  valid: "on",
};
