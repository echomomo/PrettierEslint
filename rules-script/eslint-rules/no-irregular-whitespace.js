module.exports = {
  name: "no-irregular-whitespace",
  value: ['error', {
		'skipStrings': true,
		'skipComments': false,
		'skipRegExps': true,
		'skipTemplates': true
	}],
  goodExample: `
    function foo()　{
    }
  `,
  badExample: `
    const foo = '　';
    const bar = /　/;
    const baz = \`　\`;
  `,
  resion: `
    配置：
    ['error', {
      'skipStrings': true,
      'skipComments': false,
      'skipRegExps': true,
      'skipTemplates': true
    }]
  `,
  description: `禁止使用特殊空白符（比如全角空格），除非是出现在字符串、正则表达式或模版字符串中`,
  valid: "on",
};
