module.exports = {
  name: "no-useless-escape",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "转义可以使代码更易懂",
  description: `禁止出现没必要的转义`,
  valid: "off",
};
