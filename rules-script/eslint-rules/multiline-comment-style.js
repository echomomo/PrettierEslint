module.exports = {
  name: "multiline-comment-style",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `约束多行注释的格式能写注释已经不容易了，不需要限制太多`,
  valid: "off",
};
