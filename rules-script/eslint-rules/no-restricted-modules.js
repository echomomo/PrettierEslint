module.exports = {
  name: "no-restricted-modules",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用指定的模块`,
  valid: "off",
};
