module.exports = {
  name: "operator-assignment",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `必须使用 x = x + y 而不是 x += y`,
  valid: "off",
};
