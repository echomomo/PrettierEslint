module.exports = {
  name: "no-return-await",
  value: "error",
  goodExample: `
    async function foo() {
      const b = await bar();
      return b;
    }
  `,
  badExample: `
    async function foo() {
      return await bar();
    }
  `,
  resion: "",
  description: `禁止在return语句里使用await`,
  valid: "on",
};
