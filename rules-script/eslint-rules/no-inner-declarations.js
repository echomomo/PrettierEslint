module.exports = {
  name: "no-inner-declarations",
  value: ['error', 'both'],
  goodExample: `
    if (foo) {
      const bar = function () {};
    }
  `,
  badExample: `
    if (foo) {
      function bar() {}
    }
  `,
  resion: "",
  description: `禁止在if代码块内出现函数声明`,
  valid: "on",
};
