module.exports = {
  name: "no-this-before-super",
  value: "error",
  goodExample: `
    class Foo extends Bar {
      constructor() {
        super();
        this.foo = 1;
      }
    }
  `,
  badExample: `
    class Foo extends Bar {
      constructor() {
        this.foo = 1;
        super();
      }
    }
  `,
  resion: "",
  description: `禁止在 super 被调用之前使用 this 或 super`,
  valid: "on",
};
