module.exports = {
  name: "for-direction",
  value: "error",
  goodExample: `
    for (let i = 0; i < 10; i++) {
      // do something
    }
  `,
  badExample: `
    for (let i = 0; i < 10; i--) {
      // do something
    }
  `,
  resion: "",
  description: `禁止方向错误的for循环`,
  valid: "on",
};
