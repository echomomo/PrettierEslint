module.exports = {
  name: "no-underscore-dangle",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止变量名出现下划线`,
  valid: "off",
};
