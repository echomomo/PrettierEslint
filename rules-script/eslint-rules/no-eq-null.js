module.exports = {
  name: "no-eq-null",
  value: "error",
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用foo==null，必须使用foo===null`,
  valid: "on",
};
