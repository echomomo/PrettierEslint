module.exports = {
  name: "no-negated-condition",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "否定的表达式可以把逻辑表达的更清楚",
  description: `禁止if里有否定的表达式`,
  valid: "off",
};
