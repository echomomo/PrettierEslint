module.exports = {
  name: "no-async-promise-executor",
  value: "error",
  goodExample: `
    new Promise((resolve) => {
      setTimeout(resolve, 1000);
    });
  `,
  badExample: `
    new Promise(async (resolve) => {
      setTimeout(resolve, 1000);
    });
  `,
  resion: "出现这种情况时，一般不需要使用new Promise实现异步了",
  description: `禁止将async函数做为new Promise的回调函数`,
  valid: "on",
};
