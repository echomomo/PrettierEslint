module.exports = {
  name: "no-shadow-restricted-names",
  value: "error",
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用保留字作为变量名`,
  valid: "on",
};
