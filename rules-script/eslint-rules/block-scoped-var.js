module.exports = {
  name: "block-scoped-var",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "已经禁止使用var了",
  description: `将var定义的变量视为块作用域，禁止在块外使用`,
  valid: "off",
};
