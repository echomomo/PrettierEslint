module.exports = {
  name: "no-ex-assign",
  value: "error",
  goodExample: `
    try {
    } catch (e) {
      console.error(e);
    }
  `,
  badExample: `
    try {
    } catch (e) {
      e = 10;
    }
  `,
  resion: "",
  description: `禁止将catch的第一个参数error重新赋值`,
  valid: "on",
};
