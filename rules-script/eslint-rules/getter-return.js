module.exports = {
  name: "getter-return",
  value: "error",
  goodExample: `
    const user = {
      get name() {
          return 'Alex';
      }
    };
    class User {
      get name() {
        return this.name;
      }
    }
  `,
  badExample: `
    const user = {
      get name() {
        // do something
      }
    };
    class User {
      get name() {
        return;
      }
    }
  `,
  resion: "",
  description: `getter必须有返回值，并且禁止返回空`,
  valid: "on",
};
