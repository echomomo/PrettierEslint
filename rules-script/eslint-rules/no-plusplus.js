module.exports = {
  name: "no-plusplus",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用++或--`,
  valid: "off",
};
