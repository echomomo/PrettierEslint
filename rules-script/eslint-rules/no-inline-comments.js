module.exports = {
  name: "no-inline-comments",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止在代码后添加单行注释`,
  valid: "off",
};
