module.exports = {
  name: "no-new-require",
  value: "error",
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止直接new require('foo')`,
  valid: "on",
};
