module.exports = {
  name: "guard-for-in",
  value: `off`,
  goodExample: `
    for (key in foo) {
      if (Object.prototype.hasOwnProperty.call(foo, key)) {
        doSomething(key);
      }
    }
  `,
  badExample: `
    for (key in foo) {
      doSomething(key);
    }
  `,
  resion: "",
  description: `for in内部必须有hasOwnProperty`,
  valid: "off",
};
