module.exports = {
  name: "no-sparse-arrays",
  value: "error",
  goodExample: `
    const foo = [1, 2, 3];
  `,
  badExample: `
    const foo = [1, 2, , 3];
  `,
  resion: "",
  description: `禁止在数组中出现连续的逗号`,
  valid: "on",
};
