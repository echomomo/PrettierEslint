module.exports = {
  name: "radix",
  value: ["error", "as-needed"],
  goodExample: `
    const foo = parseInt('071', 10); // 71
  `,
  badExample: `
    const foo = parseInt('071'); // 57
  `,
  resion: "",
  description: `parseInt 必须传入第二个参数`,
  valid: "on",
};
