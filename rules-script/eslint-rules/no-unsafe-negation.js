module.exports = {
  name: "no-unsafe-negation",
  value: "error",
  goodExample: `
    if (!(key in object)) {
    }
    if (!(obj instanceof SomeClass)) {
    }
  `,
  badExample: `
    if (!key in object) {
    }
    if (!obj instanceof SomeClass) {
    }
  `,
  resion: "",
  description: `禁止在 in 或 instanceof 操作符的左侧变量前使用感叹号`,
  valid: "on",
};
