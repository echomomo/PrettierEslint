module.exports = {
  name: "no-array-constructor",
  value: "error",
  goodExample: `
    const foo = [0, 1, 2];
    Array(3); // [empty × 3]
    new Array(3); // [empty × 3]
    Array(3).fill('foo'); // ["foo", "foo", "foo"]
    new Array(3).fill('foo'); // ["foo", "foo", "foo"]
  `,
  badExample: `
    const foo = Array(0, 1, 2); // [0, 1, 2]
    const bar = new Array(0, 1, 2); // [0, 1, 2]
  `,
  resion: `参数为一个时表示创建一个指定长度的数组，比较常用<br/>参数为多个时表示创建一个指定内容的数组，此时可以用数组字面量实现，不必使用构造函数`,
  description: `禁止使用Array构造函数时传入的参数超过一个`,
  valid: "on",
};
