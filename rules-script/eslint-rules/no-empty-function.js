module.exports = {
  name: "no-empty-function",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "有时需要将一个空函数设置为某个项的默认值",
  description: `不允许有空函数`,
  valid: "off",
};
