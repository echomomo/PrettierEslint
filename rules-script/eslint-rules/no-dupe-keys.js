module.exports = {
  name: "no-dupe-keys",
  value: "error",
  goodExample: `
    const foo = {
      bar: 1,
      baz: 2
    };
  `,
  badExample: `
    const foo = {
      bar: 1,
      bar: 2
    };
  `,
  resion: "",
  description: `禁止在对象字面量中出现重复的键名`,
  valid: "on",
};
