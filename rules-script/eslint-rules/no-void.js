module.exports = {
  name: "no-void",
  value: "error",
  goodExample: `
    function foo() {
      return;
    }
  `,
  badExample: `
    function foo() {
      return void 0;
    }
  `,
  resion: "",
  description: `禁止使用void`,
  valid: "on",
};
