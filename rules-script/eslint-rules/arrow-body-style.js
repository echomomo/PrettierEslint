module.exports = {
  name: "arrow-body-style",
  value: 'off',
  goodExample: ``,
  badExample: ``,
  resion: "规则限定后会在书写时变的比较难处理。",
  description: `强制或禁止在箭头函数体的周围使用大括。`,
  valid: "off",
};
