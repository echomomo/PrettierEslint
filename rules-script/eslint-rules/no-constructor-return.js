module.exports = {
  name: "no-constructor-return",
  value: "error",
  goodExample: `
    class Foo {
      constructor(bar) {
        if (!bar) {
          return;
        }
        this.bar = bar;
      }
    }
  `,
  badExample: `
    class Foo {
      constructor(bar) {
        this.bar = bar;
        return bar;
      }
    }
  `,
  resion: "",
  description: `禁止在构造函数中返回值`,
  valid: "on",
};
