module.exports = {
  name: "no-warning-comments",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止注释中出现 TODO 和 FIXME`,
  valid: "off",
};
