module.exports = {
  name: "no-undef",
  value: "error",
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用未定义的变量`,
  valid: "on",
};
