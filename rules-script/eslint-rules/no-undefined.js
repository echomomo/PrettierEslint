module.exports = {
  name: "no-undefined",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用undefined`,
  valid: "off",
};
