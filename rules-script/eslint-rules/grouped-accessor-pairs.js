module.exports = {
  name: "grouped-accessor-pairs",
  value: "error",
  goodExample: `
    const foo = {
      set bar(value) {
        this.barValue = 'bar ' + value;
      },
      get bar() {
        return this.barValue;
      },
      baz: 1
    };
  `,
  badExample: `
    const foo = {
      set bar(value) {
        this.barValue = 'bar ' + value;
      },
      baz: 1,
      get bar() {
        return this.barValue;
      }
    };
  `,
  resion: "",
  description: `setter和getter必须写在一起`,
  valid: "on",
};
