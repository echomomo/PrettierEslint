module.exports = {
  name: "no-debugger",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用debugger`,
  valid: "off",
};
