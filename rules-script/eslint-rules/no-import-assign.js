module.exports = {
  name: "no-import-assign",
  value: "error",
  goodExample: `
    import foo from 'foo';
    foo.baz = 1;

    import * as bar from 'bar';
    bar.baz.qux = 1;
  `,
  badExample: `
    import foo from 'foo';
    foo = 1;

    import * as bar from 'bar';
    bar.baz = 1;
  `,
  resion: "",
  description: `禁止对导入的模块进行赋值`,
  valid: "on",
};
