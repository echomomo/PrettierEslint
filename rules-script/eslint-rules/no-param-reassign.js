module.exports = {
  name: "no-param-reassign",
  value: "warn",
  goodExample: `
    function foo(bar_) {
      bar = bar_ || '';
    }
  `,
  badExample: `
    function foo(bar) {
      bar = bar || '';
    }
  `,
  resion: "",
  description: `*** 禁止对函数的参数重新赋值`,
  valid: "on",
};
