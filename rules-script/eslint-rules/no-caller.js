module.exports = {
  name: "no-caller",
  value: "error",
  goodExample: `
    function foo(n) {
      if (n <= 0) {
        return;
      }
      foo(n - 1);
    }
  `,
  badExample: `
    function foo(n) {
      if (n <= 0) {
        return;
      }
      arguments.callee(n - 1);
    }
  `,
  resion: "它们是已废弃的语法",
  description: `禁止使用caller或callee`,
  valid: "on",
};
