module.exports = {
  name: "no-useless-constructor",
  value: "off",
  goodExample: `
    class Foo {
      constructor() {
        doSomething();
      }
    }
    class Bar extends Foo {
      constructor(...args) {
        super(...args);
        doSomething();
      }
    }
  `,
  badExample: `
    class Foo {
      constructor() {}
    }
    class Bar extends Foo {
      constructor(...args) {
        super(...args);
      }
    }
  `,
  resion: "ES2015 为没有指定构造函数的类提供了默认构造函数，可有可无，不影响。",
  description: `禁止出现没必要的constructor`,
  valid: "off",
};
