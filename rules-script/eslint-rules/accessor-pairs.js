module.exports = {
  name: 'accessor-pairs',
  value: ['warn', {
    setWithoutGet: true,
    getWithoutSet: false
  }],
  goodExample: `
    const foo = {
      set bar(value) {
        this.barValue = 'bar ' + value;
      },
      get bar() {
        return this.barValue;
      }
    };
    const bar = {
      get foo() {
        return this.fooValue;
      }
    };
  `,
  badExample: `
    const foo = {
      set bar(value) {
        this.barValue = 'bar ' + value;
      }
    };
  `,
  resion: '',
  description: `setter必须有对应的getter，getter可以没有对应的setter`,
  valid: 'on'
};
