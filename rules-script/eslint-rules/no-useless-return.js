module.exports = {
  name: "no-useless-return",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止没必要的return`,
  valid: "off",
};
