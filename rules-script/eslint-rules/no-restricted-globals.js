module.exports = {
  name: "no-restricted-globals",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用指定的全局变量`,
  valid: "off",
};
