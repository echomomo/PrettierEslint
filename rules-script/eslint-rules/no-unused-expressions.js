module.exports = {
  name: "no-unused-expressions",
  value: ['error', {
    allowShortCircuit: true,
    allowTernary: true,
    allowTaggedTemplates: true
  }],
  goodExample: `
    'use strict';
    foo && bar();
    foo || bar();
    foo ? bar() : baz();
    foo\`bar\`;
  `,
  badExample: `
    1;
    foo;
    ('foo');
    foo && bar;
    foo || bar;
    foo ? bar : baz;
    \`bar\`;
  `,
  resion: "",
  description: `禁止无用的表达式`,
  valid: "on",
};
