module.exports = {
  name: "object-shorthand",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "有时后者可以使代码结构更清晰",
  description: `必须使用 a = {b} 而不是 a = {b: b}`,
  valid: "off",
};
