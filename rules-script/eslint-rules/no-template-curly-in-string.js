module.exports = {
  name: "no-template-curly-in-string",
  value: "error",
  goodExample: `
    const foo = 'Hello {bar}';
  `,
  badExample: `
    const foo = 'Hello \${bar}';
  `,
  resion: "",
  description: `禁止在普通字符串中出现模版字符串里的变量形式`,
  valid: "on",
};
