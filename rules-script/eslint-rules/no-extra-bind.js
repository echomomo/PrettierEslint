module.exports = {
  name: "no-extra-bind",
  value: "error",
  goodExample: `
    (function () {
      this.foo();
    }.bind(bar));
  `,
  badExample: `
    (function () {
      foo();
    }.bind(bar));
  `,
  resion: "",
  description: `禁止出现没必要的bind`,
  valid: "on",
};
