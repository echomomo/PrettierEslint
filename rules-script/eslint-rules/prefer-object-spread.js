module.exports = {
  name: "prefer-object-spread",
  value: `off`,
  goodExample: `
    const foo = { ...bar };

    // 第一个参数为变量时允许使用 Object.assign
    Object.assign(foo, baz);
  `,
  badExample: `
    const foo = Object.assign({}, bar);
  `,
  resion: "要求有点高了",
  description: `必须使用 ... 而不是 Object.assign，除非 Object.assign 的第一个参数是一个变量`,
  valid: "off",
};
