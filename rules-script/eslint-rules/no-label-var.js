module.exports = {
  name: 'no-label-var',
  value: `error`,
  goodExample: ``,
  badExample: ``,
  resion: '已经禁止使用label了',
  description: `禁止label名称与已定义的变量重复`,
  valid: 'on',
};
