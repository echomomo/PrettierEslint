module.exports = {
  name: "no-global-assign",
  value: "error",
  goodExample: `
    foo = null;
  `,
  badExample: `
    Object = null;
  `,
  resion: "",
  description: `禁止对全局变量赋值`,
  valid: "on",
};
