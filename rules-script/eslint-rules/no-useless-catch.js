module.exports = {
  name: "no-useless-catch",
  value: "error",
  goodExample: `
    doSomethingThatMightThrow();

    try {
      doSomethingThatMightThrow();
    } catch (e) {
      doSomethingBeforeRethrow();
      throw e;
    }
  `,
  badExample: `
    try {
      doSomethingThatMightThrow();
    } catch (e) {
      throw e;
    }
  `,
  resion: "这样的 catch 是没有意义的，等价于直接执行 try 里的代码",
  description: `禁止在 catch 中仅仅只是把错误 throw 出去`,
  valid: "on",
};
