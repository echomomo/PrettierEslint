module.exports = {
  name: "no-cond-assign",
  value: ['error', 'except-parens'],
  goodExample: `
    if (foo === 0) {
    }
    if (bar === (foo = 0)) {
    }
  `,
  badExample: `
    if (foo = 0) {
    }
  `,
  resion: "",
  description: `禁止在测试表达式中使用赋值语句，除非这个赋值语句被括号包起来了`,
  valid: "on",
};
