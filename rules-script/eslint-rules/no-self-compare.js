module.exports = {
  name: "no-self-compare",
  value: "error",
  goodExample: `
    if (foo === bar) {
    }
    if (isNaN(foo)) {
    }
  `,
  badExample: `
    if (foo === foo) {
    }
    if (NaN === NaN) {
    }
  `,
  resion: "",
  description: `禁止将自己与自己比较`,
  valid: "on",
};
