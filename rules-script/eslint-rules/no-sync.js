module.exports = {
  name: "no-sync",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用 node 中的同步的方法，比如 fs.readFileSync`,
  valid: "off",
};
