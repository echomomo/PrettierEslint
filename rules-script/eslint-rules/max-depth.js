module.exports = {
  name: 'max-depth',
  value: ['error',5],
  goodExample: `
  function foo() {
    if (true) {
      if (true) {
        if (true) {
          if (true) {
            if (true) {
            }
          }
        }
      }
    }
  }
  `,
  badExample: `
    function foo() {
      if (true) {
        if (true) {
          if (true) {
            if (true) {
              if (true) {
                if (true) {
                }
              }
            }
          }
        }
      }
    }
  `,
  resion: '',
  description: `代码块嵌套的深度禁止超过5层`,
  valid: 'on'
};
