module.exports = {
  name: "no-new-symbol",
  value: "error",
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用new来生成Symbol`,
  valid: "on",
};
