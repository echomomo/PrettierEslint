module.exports = {
  name: "no-bitwise",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用位运算`,
  valid: "off",
};
