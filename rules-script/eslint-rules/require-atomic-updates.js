module.exports = {
  name: "require-atomic-updates",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止将await或yield的结果做为运算符的后面项`,
  valid: "off",
};
