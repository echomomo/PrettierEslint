module.exports = {
  name: "no-iterator",
  value: "error",
  goodExample: `
    let foo = {};
    foo[Symbol.iterator] = function* () {
        yield 1;
        yield 2;
        yield 3;
    };
    console.log([...foo]);
    // [1, 2, 3]
  `,
  badExample: `
    Foo.prototype.__iterator__ = function () {
      return new FooIterator(this);
    };
  `,
  resion: "__iterator__是一个已废弃的属性，使用[Symbol.iterator]替代它",
  description: `禁止使用__iterator__`,
  valid: "on",
};
