module.exports = {
  name: "eqeqeq",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "接口不小心数据变了线上会出问题。",
  description: `必须使用===或!==，禁止使用==或!=`,
  valid: "off",
};
