module.exports = {
  name: "no-func-assign",
  value: "error",
  goodExample: `
    let foo = function () {};
    foo = 1;
  `,
  badExample: `
    function foo() {}
    foo = 1;
  `,
  resion: "",
  description: `禁止将一个函数声明重新赋值`,
  valid: "on",
};
