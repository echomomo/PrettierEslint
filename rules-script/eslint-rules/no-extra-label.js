module.exports = {
  name: "no-extra-label",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "已经禁止使用label了",
  description: `禁止出现没必要的label`,
  valid: "off",
};
