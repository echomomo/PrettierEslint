module.exports = {
  name: "no-invalid-regexp",
  value: "error",
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止在RegExp构造函数中出现非法的正则表达式`,
  valid: "on",
};
