module.exports = {
  name: "func-name-matching",
  value: ['error', 'always', {
    includeCommonJSModuleExports: false
  }],
  goodExample: `
    const foo = function () {};
    const bar = function bar() {};
  `,
  badExample: `
    const foo = function bar() {};
  `,
  resion: "",
  description: `函数赋值给变量的时候，函数名必须与变量名一致`,
  valid: "on",
};
