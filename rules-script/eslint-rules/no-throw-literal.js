module.exports = {
  name: "no-throw-literal",
  value: "error",
  goodExample: `
    throw new Error('foo');
  `,
  badExample: `
    throw 'foo';
    throw 1;
  `,
  resion: "",
  description: `禁止 throw 字面量，必须 throw 一个 Error 对象`,
  valid: "on",
};
