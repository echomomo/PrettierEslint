module.exports = {
  name: "array-callback-return",
  value: `off`,
  goodExample: `
    const foo = [1, 2, 3].map((num) => {
      return num * num;
    });
  `,
  badExample: ``,
  resion: "watch可能不需要return",
  description: `数组的方法除了forEach之外，回调函数必须有返回值`,
  valid: "off",
};
