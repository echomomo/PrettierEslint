module.exports = {
  name: "no-duplicate-case",
  value: "error",
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止在switch语句中出现重复测试表达式的case`,
  valid: "on",
};
