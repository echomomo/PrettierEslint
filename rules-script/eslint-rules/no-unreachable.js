module.exports = {
  name: "no-unreachable",
  value: "error",
  goodExample: `
    function foo() {
      return;
      // const bar = 1;
    }
  `,
  badExample: `
    function foo() {
      return;
      const bar = 1;
    }
  `,
  resion: "",
  description: `禁止在 return, throw, break 或 continue 之后还有代码`,
  valid: "on",
};
