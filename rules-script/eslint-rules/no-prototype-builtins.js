module.exports = {
  name: "no-prototype-builtins",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "hasOwnProperty 比较常用",
  description: `禁止使用 hasOwnProperty, isPrototypeOf 或 propertyIsEnumerable`,
  valid: "off",
};
