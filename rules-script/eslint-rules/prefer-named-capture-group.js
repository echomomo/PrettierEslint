module.exports = {
  name: "prefer-named-capture-group",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "正则表达式已经较难理解了，没必要强制加上命名组",
  description: `使用ES2018中的正则表达式命名组`,
  valid: "off",
};
