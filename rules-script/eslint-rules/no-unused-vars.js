module.exports = {
  name: "no-unused-vars",
  value: ['warn', {
    vars:'all',
    args:'none',
    ignoreRestSiblings: false,
    caughtErrors: 'none'
  }],
  goodExample: `
    let foo = 1;
    console.log(foo);

    function bar(baz) {}
    bar();

    const { baz, ...rest } = data;
    console.log(baz, rest);

    try {
    } catch (e) {}
  `,
  badExample: `
    let foo = 1;
    foo = 2;

    function bar(baz) {}
    
    const { baz, ...rest } = data;
  `,
  resion: "",
  description: `已定义的变量必须使用`,
  valid: "on",
};
