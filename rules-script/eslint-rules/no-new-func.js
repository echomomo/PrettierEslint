module.exports = {
  name: "no-new-func",
  value: "error",
  goodExample: `
    const foo = function (a, b) {
      return a + b;
    };
  `,
  badExample: `
    const foo = new Function('a', 'b', 'return a + b');
  `,
  resion: "这和eval是等价的",
  description: `禁止使用new Function`,
  valid: "on",
};
