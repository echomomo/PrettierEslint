module.exports = {
  name: "no-compare-neg-zero",
  value: "error",
  goodExample: `
    if (foo === 0) {
    }
  `,
  badExample: `
    if (foo === -0) {
    }
  `,
  resion: "",
  description: `禁止与负零进行比较`,
  valid: "on",
};
