module.exports = {
  name: "no-buffer-constructor",
  value: "error",
  goodExample: `
    Buffer.alloc(5);
    Buffer.from([1, 2, 3]);
  `,
  badExample: `
    new Buffer(5);
    Buffer([1, 2, 3]);
  `,
  resion: "Buffer构造函数是已废弃的语法",
  description: `禁止直接使用Buffer`,
  valid: "on",
};
