module.exports = {
  name: 'no-script-url',
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: '有些场景下还是需要用到这个',
  description: `禁止出现 location.href = 'javascript:void(0)';`,
  valid: 'off',
};
