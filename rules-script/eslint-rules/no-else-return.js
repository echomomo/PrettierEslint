module.exports = {
  name: "no-else-return",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "else中使用return可以使代码结构更清晰",
  description: `禁止在else内使用return，必须改为提前结束`,
  valid: "off",
};
