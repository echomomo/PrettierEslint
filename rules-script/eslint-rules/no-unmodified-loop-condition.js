module.exports = {
  name: 'no-unmodified-loop-condition',
  value: 'error',
  goodExample: `
    let foo = 10;
    while (foo) {
      console.log(foo);
      foo--;
    }
  `,
  badExample: `
    let foo = 10;
    while (foo) {
      console.log(foo);
    }
  `,
  resion: '',
  description: `循环内必须对循环条件中的变量有修改`,
  valid: 'on',
};
