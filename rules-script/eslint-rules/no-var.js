module.exports = {
  name: "no-var",
  value: "warn",
  goodExample: `
    let foo = 1;
    const bar = 2;
  `,
  badExample: `
    var foo = 1;
  `,
  resion: "",
  description: `禁止使用 var`,
  valid: "on",
};
