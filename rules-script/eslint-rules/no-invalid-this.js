module.exports = {
  name: "no-invalid-this",
  value: "off",
  goodExample: ``,
  badExample: ``,
  resion: "只允许在class中使用this, 暂不做强制。",
  description: `禁止在类之外的地方使用this`,
  valid: "off",
};
