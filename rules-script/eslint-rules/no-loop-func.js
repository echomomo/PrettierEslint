module.exports = {
  name: "no-loop-func",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "使用let就已经解决了这个问题了",
  description: `禁止在循环内的函数内部出现循环体条件语句中定义的变量`,
  valid: "off",
};
