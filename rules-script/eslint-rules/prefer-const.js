module.exports = {
  name: "prefer-const",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `申明后不再被修改的变量必须使用const来申明`,
  valid: "off",
};
