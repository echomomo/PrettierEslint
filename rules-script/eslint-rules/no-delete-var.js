module.exports = {
  name: "no-delete-var",
  value: `error`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `delete 的目的是删除对象的属性。使用 delete 操作删除一个变量可能会导致意外情况发生。`,
  valid: "on",
};
