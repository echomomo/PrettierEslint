module.exports = {
  name: "camelcase",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "api或文件名都不是camelcase风格的",
  description: `变量名必须是camelcase风格的`,
  valid: "off",
};
