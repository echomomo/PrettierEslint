module.exports = {
  name: "no-await-in-loop",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "要求太严格了，有时需要在循环中写await",
  description: `禁止将await写在循环里，因为这样就无法同时发送多个异步请求了`,
  valid: "off",
};
