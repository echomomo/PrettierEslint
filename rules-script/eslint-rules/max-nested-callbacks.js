module.exports = {
  name: "max-nested-callbacks",
  value: ['error', 5],
  goodExample: `
    foo(async () => {
      await bar();
      await baz();
      await qux();
    });
  `,
  badExample: `
    foo(() => {
      bar(() => {
        baz(() => {
          qux(() => {});
        });
      });
    });
  `,
  resion: "",
  description: `回调函数嵌套禁止超过5层，多了请用async await替代`,
  valid: "on",
};
