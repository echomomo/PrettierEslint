module.exports = {
  name: "no-useless-rename",
  value: "error",
  goodExample: `
    import { foo } from 'foo';
    const bar = 1;
    export { bar };
    let { baz } = foo;
  `,
  badExample: `
    import { foo as foo } from 'foo';
    const bar = 1;
    export { bar as bar };
    let { baz: baz } = foo;
  `,
  resion: "",
  description: `禁止解构赋值时出现同样名字的的重命名，比如let{foo:foo}=bar;`,
  valid: "on",
};
