module.exports = {
  name: "new-cap",
  value: ['error', {
		newIsCap:true,
		capIsNew:false,
		properties:true
	}],
  goodExample: `
    new Foo();
    new foo.Bar();
    Foo();
  `,
  badExample: `
    new foo();
    new foo.bar();
  `,
  resion: "",
  description: `new后面的类名必须首字母大写`,
  valid: "on",
};
