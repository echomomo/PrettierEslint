module.exports = {
  name: "no-octal-escape",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "编译阶段就会报错了",
  description: `禁止使用八进制的转义符`,
  valid: "off",
};
