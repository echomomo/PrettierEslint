module.exports = {
  name: "prefer-numeric-literals",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `必须使用0b11111011而不是parseInt()`,
  valid: "off",
};
