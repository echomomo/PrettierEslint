module.exports = {
  name: "max-params",
  value: ['error', 5],
  goodExample: ``,
  badExample: `
    function foo(a1, a2, a3, a4, a5, a6) {}
  `,
  resion: "",
  description: `函数的参数禁止超过5个`,
  valid: "on",
};
