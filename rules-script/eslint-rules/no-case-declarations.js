module.exports = {
  name: "no-case-declarations",
  value: "error",
  goodExample: `
    switch (foo) {
      case 1: {
        const x = 1;
        break;
      }
    }
  `,
  badExample: `
    switch (foo) {
      case 1:
        const x = 1;
        break;
    }
  `,
  resion: "",
  description: `switch的case内有变量定义的时候，必须使用大括号将case内变成一个代码块 !!!!`,
  valid: "on",
};
