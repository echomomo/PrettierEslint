module.exports = {
  name: "class-methods-use-this",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `在类的非静态方法中，必须存在对this的引用`,
  valid: "off",
};
