module.exports = {
  name: "no-path-concat",
  value: "error",
  goodExample: `
    import path from 'path';

    const foo = path.resolve(__dirname, 'foo.js');
    const bar = path.join(__filename, 'bar.js');
  `,
  badExample: `
    const foo = __dirname + '/foo.js';
    const bar = __filename + '/bar.js';
  `,
  resion: "不同平台下的路径符号不一致，建议使用 path 处理平台差异性",
  description: `禁止对 __dirname 或 __filename 使用字符串连接`,
  valid: "on",
};
