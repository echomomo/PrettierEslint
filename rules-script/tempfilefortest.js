const sCurrentDir = __dirname;

const osFs = require("fs");
const osPath = require("path");
const {exec: osExec} = require('child_process')

const { oEslintReco, oEslintAdd, oPluginVue, oEslintOther } = require('./rules-config.js');

const sTempDir = './eslint-rules';
const sTempFile = './eslint-recommended.txt';
let aFileList = []; // 本地定义项
let aREDList = []; // eslint:recommended 选项
let addRules = [];
let oFileContext = {};

// 取指定目录文件列表
function getFileLst() {
  const aTempFileList = osFs.readdirSync(sTempDir);
  if (aTempFileList[0] === '.DS_Store') {
    aTempFileList.shift();
  }
  aFileList = aTempFileList.map( (sItem) => {
    return sItem.slice(0, -3);
  });
}

// 取eslint:recommended项
function getREDList() {
  const oFileList = osFs.readFileSync(osPath.resolve(sTempFile), "utf-8");
  aREDList = oFileList.split(/[\s]+/).filter((sItem) => { return sItem !== '--off'});
}

// 对比 eslint:recommended 和 本地数据
function diff (){
  aFileList.map(sItem => {
    if(aREDList.indexOf(sItem) === -1){
      const oGetCon = getFileContext(sItem);
      if (oGetCon.valid === 'on'){
        console.log(`'${oGetCon.name}', // ${oGetCon.description}`)
      }
    }
  })
}

// 遍历读取数组里的项目内容
function getFileContext(sItem) {
  if (sItem === ".DS_Store") { return false; }
  const sTempPath = osPath.resolve(osPath.join(sTempDir, sItem) + '.js');
  let oRequVal = require(osPath.resolve(sTempPath));
  return oRequVal;
}

// 查看recommended开启情况
function checkRecommended() {
  let oRequVal
  aREDList.map((sItem) => {
    oRequVal = getFileContext(sItem);
    oFileContext[sItem] = oRequVal;
  });
  const aKeys = Object.keys(oFileContext);
  for (let i = 0, iLen = Object.keys(oFileContext).length; i < iLen; i++) {
    if(oFileContext[aKeys[i]]['valid'] === 'off'){
      console.log("oFileContext[i]", oFileContext[aKeys[i]]['name'])
    }
  }
}

// 遍历文件列表加载缓存
function requireAll() {
  aFileList.map((sItem) => {
    const sTempPath = osPath.resolve(osPath.join(sTempDir, sItem) + '.js');    
    if(!fsExistsSync(sTempPath)){
      return false;
    }
    // if (sItem !== ".DS_Store") {
      let oRequVal = require(sTempPath);
      oFileContext[sItem] = oRequVal;
    // }
  });
}

// 检测文件或者文件夹存在
function fsExistsSync(sPath) {
  try {
    osFs.accessSync(sPath, osFs.F_OK);
  } catch (e) {
    return false;
  }
  return true;
}


// getFileLst()
// getREDList()
// checkRecommended()
// diff()

// const aCheckList = [...Object.keys(oEslintReco), ...Object.keys(oEslintAdd)];


aFileList = oEslintOther;
requireAll()

aFileList.map( (sItem) =>{
  console.log(`'${oFileContext[sItem]["name"]}': '${oFileContext[sItem]["valid"]}', // ${oFileContext[sItem]["description"]}`)
})