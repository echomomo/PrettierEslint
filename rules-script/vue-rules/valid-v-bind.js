module.exports = {
  name: "vue/valid-v-bind",
  value: `error`,
  goodExample: `
    <template>
      <div>
        <div v-bind="foo"></div>
        <div v-bind:aaa="foo"></div>
        <div :aaa="foo"></div>
        <div :aaa.prop="foo"></div>
      </div>
    </template>
  `,
  badExample: `
    <template>
      <div>
        <div v-bind></div>
        <div :aaa></div>
        <div v-bind:aaa.bbb="foo"></div>
      </div>
    </template>
  `,
  resion: "",
  description: `v-bind 指令必须合法`,
  valid: "on",
};
