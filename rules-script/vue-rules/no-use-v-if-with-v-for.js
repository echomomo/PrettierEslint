module.exports = {
  name: "vue/no-use-v-if-with-v-for",
  value: `error`,
  goodExample: `
    <template>
      <ul v-if="complete">
        <TodoItem v-for="todo in todos" />
      </ul>
    </template>
  `,
  badExample: `
    <template>
      <TodoItem v-if="complete" v-for="todo in todos" />
    </template>
  `,
  resion: "",
  description: `*** 禁止在同一个元素上使用 v-if 和 v-for 指令`,
  valid: "on",
};
