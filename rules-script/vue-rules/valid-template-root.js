module.exports = {
  name: "vue/valid-template-root",
  value: `error`,
  goodExample: `
    <template>
      <div></div>
    </template>
  `,
  badExample: `
    <template>
        <div></div>
        <div></div>
    </template>
  `,
  resion: "",
  description: `template 的根节点必须合法`,
  valid: "on",
};
