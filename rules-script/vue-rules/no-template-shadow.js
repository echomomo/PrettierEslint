module.exports = {
  name: "vue/no-template-shadow",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `模版中的变量名禁止与前一个作用域重名`,
  valid: "off",
};
