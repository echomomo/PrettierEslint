module.exports = {
  name: "vue/no-static-inline-styles",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用 style 属性`,
  valid: "off",
};
