module.exports = {
  name: "vue/no-unused-components",
  value: `warn`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止定义在 components 中的组件未使用`,
  valid: "on",
};
