module.exports = {
  name: "vue/no-duplicate-attributes",
  value: ['warn', {
    allowCoexistClass: false,
    allowCoexistStyle: false
  }],
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止出现重复的属性`,
  valid: "on",
};
