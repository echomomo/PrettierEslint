module.exports = {
  name: "vue/use-v-on-exact",
  value: `error`,
  goodExample: `
    <button
      @click="foo" 
      :click="foo"
    ></button>
    <button
      v-on:click.exact="foo"
      v-on:click.ctrl="foo"
    ></button>
  `,
  badExample: `
    <button
      v-on:click="foo"
      v-on:click.ctrl="foo"
    ></button>
  `,
  resion: "",
  description: `当一个节点上出现两个 v-on:click 时，其中一个必须为 exact`,
  valid: "on",
};
