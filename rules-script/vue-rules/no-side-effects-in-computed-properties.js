module.exports = {
  name: "vue/no-side-effects-in-computed-properties",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止在计算属性中对属性修改`,
  valid: "off",
};
