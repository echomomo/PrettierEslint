module.exports = {
  name: "vue/camelcase",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "很多 PHP API 不是 camelcase 风格的",
  description: `变量名必须是 camelcase 风格的`,
  valid: "off",
};
