module.exports = {
  name: "vue/require-render-return",
  value: `warn`,
  goodExample: `
    export default {
      render(h) {
        return h('div', 'hello');
      }
    };
  `,
  badExample: `
    export default {
      render(h) {
        if (foo) {
          return h('div', foo);
        }
      }
    };
  `,
  resion: "",
  description: `render 函数必须有返回值`,
  valid: "on",
};
