module.exports = {
  name: "vue/v-on-function-call",
  value: `error`,
  goodExample: `
    <template>
      <button v-on:click="closeModal">
        Close
      </button>
    </template>
  `,
  badExample: `
    <template>
      <button v-on:click="closeModal()">
        Close
      </button>
    </template>
  `,
  resion: "",
  description: `禁止在 v-on 的值中调用函数`,
  valid: "on",
};
