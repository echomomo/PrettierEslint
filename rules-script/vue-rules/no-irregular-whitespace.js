module.exports = {
  name: "vue/no-irregular-whitespace",
  value: ['warn', {
    skipStrings: true,
    skipComments: false,
    skipRegExps: true,
    skipTemplates: true,
    skipHTMLTextContents: true,
  }],
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止使用特殊空白符（比如全角空格），除非是出现在字符串、正则表达式、模版字符串中或 HTML 内容中`,
  valid: "on",
};
