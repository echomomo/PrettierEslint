module.exports = {
  name: "vue/no-textarea-mustache",
  value: `error`,
  goodExample: `
    <template>
      <textarea v-model="message" />
    </template>
  `,
  badExample: `
    <template>
      <textarea>{{ message }}</textarea>
    </template>
  `,
  resion: "",
  description: `禁止在 <textarea> 中出现模版语法 {{message}}`,
  valid: "on",
};
