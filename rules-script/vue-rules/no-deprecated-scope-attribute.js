module.exports = {
  name: "vue/no-deprecated-scope-attribute",
  value: `error`,
  goodExample: `
    <template>
      <ListComponent>
        <template v-slot:name="props">
          {{ props.title }}
        </template>
        <template slot="name" slot-scope="props">
          {{ props.title }}
        </template>
      </ListComponent>
    </template>
  `,
  badExample: `
    <template>
      <ListComponent>
        <template slot="name" scope="props">
          {{ props.title }}
        </template>
      </ListComponent>
    </template>
  `,
  resion: "",
  description: `禁用已废弃的 scope 属性`,
  valid: "on",
};
