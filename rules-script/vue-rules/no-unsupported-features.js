module.exports = {
  name: "vue/no-unsupported-features",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `当你的 vue 版本较老时，禁用还未支持的语法`,
  valid: "off",
};
