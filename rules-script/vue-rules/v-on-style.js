module.exports = {
  name: "vue/v-on-style",
  value: `off`,
  goodExample: `
    <template>
      <div @click="foo" />
    </template>
  `,
  badExample: `
    <template>
      <div v-on:click="foo" />
    </template>
  `,
  resion: "",
  description: `使用缩写的 @click 而不是 v-on:click`,
  valid: "off",
};
