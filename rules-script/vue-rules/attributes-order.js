module.exports = {
  name: "vue/attributes-order",
  value: `warn`,
  goodExample: `
    <template>
      <div
        is="header"
        v-for="item in items"
        v-if="!visible"
        v-once
        id="uniqueID"
        ref="header"
        v-model="headerData"
        my-prop="prop"
        @click="functionCall"
        v-text="textContent"
      />
    </template>
  `,
  badExample: `
    <template>
      <div
        ref="header"
        v-for="item in items"
        v-once
        id="uniqueID"
        v-model="headerData"
        my-prop="prop"
        v-if="!visible"
        is="header"
        @click="functionCall"
        v-text="textContent"
      />
    </template>
  `,
  resion: "",
  description: `标签属性必须按规则排序`,
  valid: "on",
};
