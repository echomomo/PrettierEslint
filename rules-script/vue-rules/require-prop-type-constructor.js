module.exports = {
  name: "vue/require-prop-type-constructor",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `props 的取值必须是基本类型的构造函数，而不是字符串`,
  valid: "off",
};
