module.exports = {
  name: "vue/component-definition-name-casing",
  value: 'off', // ["PascalCase" | "kebab-case"]
  goodExample: `
    <script>
    export default {
      name: 'FooBar'
    };
    </script>
  `,
  badExample: `
    <script>
    export default {
      name: 'foo-bar'
    };
    </script>
  `,
  resion: "这是官方建议的规范, 此处强改变动比较大，只做推荐。",
  description: `组件的 name 属性必须符合 PascalCase`,
  valid: "on",
};
