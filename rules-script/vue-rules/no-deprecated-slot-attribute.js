module.exports = {
  name: "vue/no-deprecated-slot-attribute",
  value: `off`,
  goodExample: `
    <template>
      <ListComponent>
        <template v-slot:name>
          {{ props.title }}
        </template>
      </ListComponent>
    </template>
  `,
  badExample: `
    <template>
      <ListComponent>
        <template slot="name">
          {{ props.title }}
        </template>
      </ListComponent>
    </template>
  `,
  resion: "兼容旧版本，不强制，官方3.0才会废弃此功能。",
  description: `使用 v-slot 替代已废弃的 slot`,
  valid: "off",
};
