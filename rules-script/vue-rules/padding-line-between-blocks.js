module.exports = {
  name: "vue/padding-line-between-blocks",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "代码格式问题，由 Prettier 解决",
  description: `<template> <script> <style> 之间必须由空行 `,
  valid: "off",
};
