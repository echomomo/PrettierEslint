module.exports = {
  name: "vue/no-template-key",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止 <template> 使用 key 属性`,
  valid: "off",
};
