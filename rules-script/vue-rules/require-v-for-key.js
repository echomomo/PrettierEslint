module.exports = {
  name: "vue/require-v-for-key",
  value: `warn`,
  goodExample: `
    <template>
      <div v-for="todo in todos" :key="todo.id" />
    </template>
  `,
  badExample: `
    <template>
      <div v-for="todo in todos" />
    </template>
  `,
  resion: "",
  description: `v-for 指令的元素必须有 v-bind:key`,
  valid: "on",
};
