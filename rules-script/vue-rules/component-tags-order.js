module.exports = {
  name: "vue/component-tags-order",
  value: ['warn', {
    order: ['template', 'script', 'style']
  }],
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `组件中必须按照 <template>, <script>, <style> 排序，标签的顺序保持一致`,
  valid: "on",
};
