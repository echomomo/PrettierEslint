module.exports = {
  name: "vue/valid-v-cloak",
  value: `error`,
  goodExample: `
    <template>
      <div v-cloak />
    </template>
  `,
  badExample: `
    <template>
      <div v-cloak:aaa />
      <div v-cloak.bbb />
      <div v-cloak="ccc" />
    </template>
  `,
  resion: "",
  description: `v-cloak 指令必须合法`,
  valid: "on",
};
