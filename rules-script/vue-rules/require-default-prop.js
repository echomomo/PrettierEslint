module.exports = {
  name: "vue/require-default-prop",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `props 如果不是 required 的字段，必须有默认值`,
  valid: "off",
};
