module.exports = {
  name: "vue/no-parsing-error",
  value: ['warn', {
    'x-invalid-end-tag': false, // 处理iView
    'invalid-first-character-of-tag-name': false, // 处理 {{ (test.tag < 1) ? '状态是大于1的' : '状态是小于1的' }} “<” 报错问题
  }],
  goodExample: `
    <div
      class="text"
      v-text="(totalNum < 100) ? totalNum : '99+'"
    >
    </div>
  `,
  badExample: `
    <div class="text">
      {{ totalNum < 100 ? totalNum : '99+' }}
    </div>
  `,
  resion: "",
  description: `*** 禁止出现语法错误`,
  valid: "on",
};
