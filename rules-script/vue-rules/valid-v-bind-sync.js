module.exports = {
  name: "vue/valid-v-bind-sync",
  value: `error`,
  goodExample: `
    <template>
      <MyComponent v-bind:aaa.sync="foo" />
      <MyComponent :aaa.sync="foo" />

      <div v-for="todo in todos">
        <MyComponent v-bind:aaa.sync="todo.name" />
        <MyComponent :aaa.sync="todo.name" />
      </div>
    </template>
  `,
  badExample: `
    <template>
      <MyComponent v-bind:aaa.sync="foo + bar" />
      <MyComponent :aaa.sync="foo + bar" />

      <input v-bind:aaa.sync="foo" />
      <input :aaa.sync="foo" />

      <div v-for="todo in todos">
        <MyComponent v-bind:aaa.sync="todo" />
        <MyComponent :aaa.sync="todo" />
      </div>
    </template>
  `,
  resion: "",
  description: `v-bind:foo.sync 指令必须合法`,
  valid: "on",
};
