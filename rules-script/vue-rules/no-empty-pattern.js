module.exports = {
  name: "vue/no-empty-pattern",
  value: `error`,
  goodExample: `
    <template>
      <div
        @click="
          () => {
            const { bar } = foo;
          }
        "
      ></div>
    </template>
  `,
  badExample: `
    <template>
      <div
        @click="
          () => {
            const {} = foo;
          }
        "
      ></div>
    </template>
  `,
  resion: "",
  description: `禁止解构赋值中出现空 {} 或 []`,
  valid: "on",
};
