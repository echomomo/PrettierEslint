module.exports = {
  name: "vue/max-attributes-per-line",
  value: ['warn', {
    'singleline': 4,
    'multiline': {
      'max': 3,
      'allowFirstLine': false
    }
  }],
  goodExample: `
    <img
      src="https://vuejs.org/images/logo.png"
      alt="Vue Logo"
    />
    <MyComponent
      foo="a"
      bar="b"
      baz="c"
    />
  `,
  badExample: `
    <img src="https://vuejs.org/images/logo.png" alt="Vue Logo" style="..." width="" />
    <MyComponent foo="a" bar="b" baz="c" bad="d" />
  `,
  resion: "如果1个一行，后台化的UI界面会很难看清结构。",
  description: `强制每行属性的最大数量，最多不超4个。`,
  valid: "on",
};
