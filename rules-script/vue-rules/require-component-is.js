module.exports = {
  name: "vue/require-component-is",
  value: `warn`,
  goodExample: `
    <template>
      <component :is="currentComponnet" />
      <component v-bind:is="currentComponnet" />
    </template>
  `,
  badExample: `
    <template>
      <component />
      <component is="currentComponnet" />
    </template>
  `,
  resion: "",
  description: `<component> 必须有绑定的组件`,
  valid: "on",
};
