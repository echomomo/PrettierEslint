module.exports = {
  name: "vue/order-in-components",
  value: `warn`,
  goodExample: `
    export default {
      name: 'World',
      props: {
        bar: Number
      },
      data() {
        return {
          foo: 'Hello'
        };
      }
    };
  `,
  badExample: `
    export default {
      data() {
        return {
          foo: 'Hello'
        };
      },
      props: {
        bar: Number
      },
      name: 'World'
    };
  `,
  resion: "",
  description: `组件的属性必须为一定的顺序`,
  valid: "on",
};
