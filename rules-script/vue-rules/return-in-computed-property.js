module.exports = {
  name: "vue/return-in-computed-property",
  value: `error`,
  goodExample: `
    export default {
      computed: {
        foo() {
          if (this.bar) {
            return this.bar;
          }
          return this.baz;
        }
      }
    };
  `,
  badExample: `
    export default {
      computed: {
        foo() {
          if (this.bar) {
            return this.bar;
          }
        }
      }
    };
  `,
  resion: "",
  description: `计算属性必须有返回值`,
  valid: "on",
};
