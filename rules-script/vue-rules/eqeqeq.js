module.exports = {
  name: "vue/eqeqeq",
  value: `off`,
  goodExample: `
    <template>
      <div :attr="foo === 1" />
    </template>
  `,
  badExample: `
    <template>
      <div :attr="foo == 1" />
    </template>
  `,
  resion: "接口数据有可能不规范，不强制。",
  description: `必须使用 === 或 !==，禁止使用 == 或 !=`,
  valid: "off",
};
