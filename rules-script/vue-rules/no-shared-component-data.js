module.exports = {
  name: "vue/no-shared-component-data",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `组件的 data 属性的值必须是一个函数`,
  valid: "off",
};
