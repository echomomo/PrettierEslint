module.exports = {
  name: "vue/no-boolean-default",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `禁止给布尔值 props 添加默认值`,
  valid: "off",
};
