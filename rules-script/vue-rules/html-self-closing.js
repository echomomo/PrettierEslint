module.exports = {
  name: "vue/html-self-closing",
  value: ['warn', {
		'html': {
			'void': 'always',
			'normal': 'any',
			'component': 'any'
		},
		'svg': 'always',
		'math': 'always'
	}],
	goodExample: `
		<!-- 在单文件组件、字符串模板和 JSX 中 -->
		<MyComponent/>
		<!-- 在 DOM 模板中 -->
		<my-component></my-component>
	`,
	badExample: `
		<!-- 在单文件组件、字符串模板和 JSX 中 -->
		<MyComponent></MyComponent>
		<!-- 在 DOM 模板中 -->
		<my-component/>
	`,
  resion: "",
  description: `执行自动关闭样式`,
  valid: "on",
};
