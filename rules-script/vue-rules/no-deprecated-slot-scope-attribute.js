module.exports = {
  name: "vue/no-deprecated-slot-scope-attribute",
  value: `off`,
  goodExample: `
    <template>
      <ListComponent>
        <template v-slot="props">
          {{ props.title }}
        </template>
      </ListComponent>
    </template>
  `,
  badExample: `
    <template>
      <ListComponent>
        <template slot-scope="props">
          {{ props.title }}
        </template>
      </ListComponent>
    </template>
  `,
  resion: "",
  description: `禁用已废弃的 slot-scope`,
  valid: "off",
};
