module.exports = {
  name: "vue/no-async-in-computed-properties",
  value: `error`,
  goodExample: `
    export default {
      computed: {
        foo() {
          return someFunc();
        }
      }
    };
  `,
  badExample: `
    export default {
      computed: {
        foo: async function () {
          return await someFunc();
        },
        bar() {
          return new Promise();
        },
        baz() {
          setTimeout(() => {}, 0);
        }
      }
    };
  `,
  resion: "",
  description: `计算属性禁止包含异步方法`,
  valid: "on",
};
