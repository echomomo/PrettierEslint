module.exports = {
  name: "vue/comment-directive",
  value: `warn`,
  goodExample: `
    <template>
      <!-- eslint-disable-next-line vue/eqeqeq -->
      <div :attr="foo == 1" />
    </template>
  `,
  badExample: ``,
  resion: "",
  description: `支持在模版中使用 eslint-disable-next-line 等注释`,
  valid: "on",
};
