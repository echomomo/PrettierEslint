module.exports = {
  name: "vue/require-valid-default-prop",
  value: `off`,
  goodExample: ``,
  badExample: ``,
  resion: "",
  description: `prop 的默认值必须匹配它的类型`,
  valid: "off",
};
